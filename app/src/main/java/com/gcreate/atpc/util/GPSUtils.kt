package com.gcreate.atpc.util

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat


class GPSUtils private constructor(context: Context) {

    private val mContext = context
    private var locationManager: LocationManager? = null


    companion object {
        private var instance: GPSUtils? = null
        fun getInstance(context: Context): GPSUtils? {
            if (instance == null) {
                instance = GPSUtils(context)
            }
            return instance
        }
    }

    /**
     * 獲取經緯度
     *
     * @return
     */

    fun getLngAndLat(onLocationResultListener: OnLocationResultListener?): String? {
        mOnLocationListener = onLocationResultListener
        var locationProvider: String? = null

        locationManager = mContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        //獲取所有可用的位置提供器
        val providers = locationManager!!.getProviders(true)
        when {
            providers.contains(LocationManager.GPS_PROVIDER) -> {
                //如果是GPS
                locationProvider = LocationManager.GPS_PROVIDER
            }
            providers.contains(LocationManager.NETWORK_PROVIDER) -> {
                //如果是Network
                locationProvider = LocationManager.NETWORK_PROVIDER
            }
            else -> {
                val i = Intent()
                i.action = Settings.ACTION_LOCATION_SOURCE_SETTINGS
                mContext.startActivity(i)
                return null
            }
        }

        //獲取Location
        if (checkPermission()) {
            val location: Location? = locationManager!!.getLastKnownLocation(locationProvider)
            if (location != null) {
                //不為空,顯示地理位置經緯度
                if (mOnLocationListener != null) {
                    mOnLocationListener!!.onLocationResult(location)
                }
            }
            //監視地理位置變化
            locationManager!!.requestLocationUpdates(locationProvider , 3000 , 1f , locationListener)
        }

        return null
    }

    private var locationListener: LocationListener = object : LocationListener {
        // Provider的狀態在可用、暫時不可用和無服務三個狀態直接切換時觸發此函數
        override fun onStatusChanged(provider: String , status: Int , extras: Bundle) {}

        // Provider被enable時觸發此函數，比如GPS被打開
        override fun onProviderEnabled(provider: String) {}

        // Provider被disable時觸發此函數，比如GPS被關閉
        override fun onProviderDisabled(provider: String) {}

        //當坐標改變時觸發此函數，如果Provider傳進相同的坐標，它就不會被觸發
        override fun onLocationChanged(location: Location) {
            if (mOnLocationListener != null) {
                mOnLocationListener!!.onLocationChange(location)
            }
        }
    }

    fun removeListener() {
        locationManager!!.removeUpdates(locationListener)
    }

    private var mOnLocationListener: OnLocationResultListener? = null

    interface OnLocationResultListener {
        fun onLocationResult(location: Location?)
        fun onLocationChange(location: Location?)
    }

    private fun checkPermission(): Boolean {
        val hasGone: Int = ActivityCompat.checkSelfPermission(mContext , Manifest.permission.ACCESS_FINE_LOCATION)
        return hasGone == PackageManager.PERMISSION_GRANTED
    }
}