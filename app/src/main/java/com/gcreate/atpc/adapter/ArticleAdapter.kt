package com.gcreate.atpc.adapter

import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.atpc.R
import com.gcreate.atpc.webAPI.Post

class ArticleAdapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<Post>) : BaseQuickAdapter<Post , BaseViewHolder>(layoutResId , data), LoadMoreModule {



    override fun convert(holder: BaseViewHolder , item: Post) {
        holder.setText(R.id.tv_article_date , item.post_date)
        holder.setText(R.id.tv_article_content , item.post_title)


        val itemImage: ImageView = holder.getView(R.id.img_article)
        Glide.with(mActivity).load(item.post_img_url).error(R.mipmap.image_demo).transform(CenterCrop() , RoundedCorners(8)).into(itemImage)

        holder.itemView.setBackgroundResource(android.R.color.transparent)


    }

}


