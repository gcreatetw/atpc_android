package com.gcreate.atpc.adapter

import androidx.fragment.app.FragmentActivity
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.atpc.R
import com.gcreate.atpc.webAPI.Room

class ClinicRoomAdapter(private val mActivity: FragmentActivity, layoutResId: Int, data: MutableList<Room>) : BaseQuickAdapter<Room, BaseViewHolder>(layoutResId, data), LoadMoreModule {



    override fun convert(holder: BaseViewHolder, item: Room) {
        holder.setText(R.id.tv_clinic_room_currentNumber, item.currentNum.toString())
        holder.setText(R.id.tv_clinic_room_name,item.name)
    }



}


