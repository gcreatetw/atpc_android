package com.gcreate.atpc.adapter

import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.atpc.R
import com.gcreate.atpc.listener.ItemClickListener
import com.gcreate.atpc.webAPI.ClinicsInfo

class ClinicListAdapter(private val mActivity: FragmentActivity, layoutResId: Int, data: MutableList<ClinicsInfo>) : BaseQuickAdapter<ClinicsInfo, BaseViewHolder>(layoutResId, data), LoadMoreModule {

    private var itemClickListener: ItemClickListener? = null

    override fun convert(holder: BaseViewHolder, item: ClinicsInfo) {

        val itemImage: ImageView = holder.getView(R.id.imageClinic)
        Glide.with(mActivity).load(item.clinic_img_url).into(itemImage)

        holder.setText(R.id.textClinicName, item.clinic_name)
        holder.setText(R.id.textClinicAddr, "地址:" + item.clinic_adress)
        holder.setText(R.id.textClinicPhone, "電話:" + item.clinic_tel)


        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(holder.layoutPosition)
        }
    }

    // 提供set方法
    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}


