package com.gcreate.atpc.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.FragmentFavoriteBinding
import com.gcreate.atpc.listener.RefreshBackgroundListenerManager
import com.gcreate.atpc.storagedData.StorageDataMaintain
import com.gcreate.atpc.webAPI.ApiObjectClinicDetail
import com.google.android.material.snackbar.Snackbar

class ClinicFavListAdapter(private val mActivity: Activity, private val binding: FragmentFavoriteBinding, private var itemsBeanLists2: List<ApiObjectClinicDetail>) : RecyclerView.Adapter<ClinicFavListAdapter.ViewHolder>() {
    var listener: View.OnClickListener? = null
    private var deletedNameCard: ApiObjectClinicDetail? = null
    private var onItemClickListener: OnItemClickListener? = null

    interface OnItemClickListener {
        fun onItemClick(view: View?, position: Int)
    }

    fun setOnItemClickListener(listener: OnItemClickListener?) {
        onItemClickListener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.card_clinic, parent, false)
        v.setBackgroundColor(ContextCompat.getColor(mActivity, android.R.color.transparent))
        if (listener != null) {
            v.setOnClickListener { view: View? -> listener!!.onClick(view) }
        }
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_clinic_name.text = itemsBeanLists2[position].clinic_name
        holder.tv_clinic_address.text = "地址 : " + itemsBeanLists2.get(position).clinic_address
        holder.tv_clinic_phone.text = "電話 : " + itemsBeanLists2.get(position).clinic_tel
        Glide.with(mActivity).load(itemsBeanLists2[position].clinic_img_url).into(holder.img_clinic)
        holder.itemView.setOnClickListener { onItemClickListener!!.onItemClick(holder.itemView, position) }
    }

    override fun getItemCount(): Int {

        return if (itemsBeanLists2.isNullOrEmpty() || itemsBeanLists2.size == 0 ){
            0
        }else{
            itemsBeanLists2.size
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tv_clinic_name: TextView = itemView.findViewById(R.id.textClinicName)
        val tv_clinic_address: TextView = itemView.findViewById(R.id.textClinicAddr)
        val tv_clinic_phone: TextView = itemView.findViewById(R.id.textClinicPhone)
        val img_clinic: ImageView = itemView.findViewById(R.id.imageClinic)

    }

    private fun refresh(itemsBeanLists: List<ApiObjectClinicDetail>) {
        itemsBeanLists2 = itemsBeanLists
        notifyDataSetChanged()
        RefreshBackgroundListenerManager.getInstance().sendBroadCast()
    }

    fun deleteItem(position: Int) {
        //  刪除滑動的診所
        deletedNameCard = itemsBeanLists2.get(position)
        StorageDataMaintain.removeFromFavList(position)
        refresh(StorageDataMaintain.favClinicList)
        showUndoSnackBar()
    }

    private fun showUndoSnackBar() {
        val snackBar = Snackbar.make(binding.flContent, "已刪除", Snackbar.LENGTH_LONG)
        snackBar.setAction("還原") { unDeleteItem() }
        snackBar.show()
    }

    private fun unDeleteItem() {
        StorageDataMaintain.addToFavList(deletedNameCard)
        refresh(itemsBeanLists2)
    }

}