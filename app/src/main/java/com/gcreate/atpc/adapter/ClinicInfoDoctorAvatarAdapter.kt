package com.gcreate.atpc.adapter

import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.atpc.R
import com.gcreate.atpc.listener.ItemClickListener
import com.gcreate.atpc.webAPI.ClinicDoctor

class ClinicInfoDoctorAvatarAdapter(private val mActivity: FragmentActivity, layoutResId: Int, data: MutableList<ClinicDoctor>) : BaseQuickAdapter<ClinicDoctor, BaseViewHolder>(layoutResId, data), LoadMoreModule {

    private var itemClickListener: ItemClickListener? = null

    override fun convert(holder: BaseViewHolder, item: ClinicDoctor) {

        val itemImage: ImageView = holder.getView(R.id.img_doctor_avatar)
        Glide.with(mActivity).load(item.clinic_doctor_picture_url).into(itemImage)

        holder.setText(R.id.tv_doctor_name, item.clinic_doctor_name)

        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(holder.layoutPosition)
        }
    }

    // OnClickListener
    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}


