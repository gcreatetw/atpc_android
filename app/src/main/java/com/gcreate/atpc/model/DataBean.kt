package com.gcreate.atpc.model

import com.gcreate.atpc.R

class DataBean {

    var imageRes: Int? = null
    var imageUrl: String? = null
    var title: String? = null
    var viewType: Int = 0
    lateinit var cityName_ch: String
    lateinit var cityName_en: String
    var cityID: Int = 0

    constructor(imageRes: Int?, title: String?, viewType: Int) {
        this.imageRes = imageRes
        this.title = title
        this.viewType = viewType

    }

    constructor(imageUrl: String?, title: String?, viewType: Int) {
        this.imageUrl = imageUrl
        this.title = title
        this.viewType = viewType
    }

    //    constructor(imageUrl: String? , title: String? , viewType: Int) {
    //        this.imageUrl = imageUrl
    //        this.title = title
    //        this.viewType = viewType
    //    }

    constructor(imageRes: Int?, cityName_ch: String, cityName_en: String, cityID: Int) {
        this.imageRes = imageRes
        this.cityName_ch = cityName_ch
        this.cityName_en = cityName_en
        this.cityID = cityID
    }

    companion object {

        //测试数据，如果图片链接失效请更换
        //        val bannerImageWithoutWeb: List<DataBean>
        //            get() {
        //                val list: MutableList<DataBean> = ArrayList()
        //                list.add(DataBean(Global.pdfToBitmap(mContext, )R.mipmap.home_bn01 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn02 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn03 , null , 1))
        //                return list
        //            }

        fun bannerImages(): MutableList<DataBean> {
            val list: MutableList<DataBean> = ArrayList()
            list.add(DataBean(R.raw.banner_default, null, 1))
            return list
        }

        fun taiwanCity(): List<String> {
            return mutableListOf("基隆市", "台北市", "新北市", "桃園市", "新竹市",
                "新竹縣", "苗栗縣", "台中市", "彰化縣", "南投縣", "雲林縣", "嘉義市", "嘉義縣", "台南市",
                "高雄市", "屏東縣", "宜蘭縣", "台東縣", "花蓮縣", "澎湖縣", "金門縣", "連江縣")
        }

    }





}
