package com.gcreate.atpc.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectArticle(
    @SerializedName("posts")
    val posts: MutableList<Post>,
    @SerializedName("posts_page_banner")
    val posts_page_banner: MutableList<String>
)

data class Post(
    @SerializedName("post_date")
    val post_date: String,
    @SerializedName("post_img_url")
    val post_img_url: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)