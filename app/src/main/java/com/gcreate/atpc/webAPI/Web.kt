package com.gcreate.atpc.webAPI

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Web {

    companion object {
        //  ATPC server
        private const val DOMAIN = "demo.gcreate.com.tw/gc_emh/wp-json"
        private const val SERVER_Url = "http://$DOMAIN/"
        lateinit var ATPC_API: APIs

        //  RightTime server
        private var HOST = "app.right-time.com.tw"
        private var PORT = "443"
        private val RightTImeSERVER_Url = "https://$HOST:$PORT/api/"
        lateinit var RightTime_API: RightTimeAPIs

        fun initAtpcAPI() {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                val builder = chain.request().newBuilder()
                val build = builder.build()
                chain.proceed(build)
            }.connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

            ATPC_API = Retrofit.Builder().baseUrl(SERVER_Url).addConverterFactory(GsonConverterFactory.create()).client(client).build()
                .create(APIs::class.java)
        }


        fun initRightTimeAPI() {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val RTclient = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                val builder = chain.request().newBuilder()
                val build = builder.build()
                chain.proceed(build)
            }.connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()

            RightTime_API = Retrofit.Builder().baseUrl(RightTImeSERVER_Url).addConverterFactory(GsonConverterFactory.create()).client(RTclient).build()
                .create(RightTimeAPIs::class.java)
        }

    }

}