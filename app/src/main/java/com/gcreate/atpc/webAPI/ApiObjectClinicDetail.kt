package com.gcreate.atpc.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectClinicDetail(

    @SerializedName("is_use_right_time_system")
    val is_use_right_time_system: Boolean,
    @SerializedName("right_time_clinic_id")
    val right_time_clinic_id: String,
    @SerializedName("clinic_id")
    val clinic_id: Int,
    @SerializedName("business_hours")
    val business_hours: BusinessHours,
    @SerializedName("clinic_description")
    val clinic_description: String,
    @SerializedName("clinic_tel")
    val clinic_tel: String,
    @SerializedName("clinic_address")
    val clinic_address: String,
    @SerializedName("clinic_website")
    val clinic_website: String,
    @SerializedName("clinic_doctors")
    val clinic_doctors: MutableList<ClinicDoctor>,
    @SerializedName("clinic_fb")
    val clinic_fb: String,
    @SerializedName("clinic_img_url")
    val clinic_img_url: String,
    @SerializedName("clinic_line")
    val clinic_line: String,
    @SerializedName("clinic_month_img_url")
    val clinic_month_img_url: String,
    @SerializedName("clinic_name")
    val clinic_name: String,
    @SerializedName("duty_doctors")
    val duty_doctors: List<DutyDoctor>)

data class BusinessHours(
    @SerializedName("afternoon")
    val afternoon: String,
    @SerializedName("evening")
    val evening: String,
    @SerializedName("morning")
    val morning: String)

data class ClinicDoctor(
    @SerializedName("clinic_doctor_id")
    val clinic_doctor_id: Int,
    @SerializedName("clinic_doctor_name")
    val clinic_doctor_name: String,
    @SerializedName("clinic_doctor_picture_url")
    val clinic_doctor_picture_url: String)

data class DutyDoctor(
    @SerializedName("afternoon_doctor")
    val afternoon_doctor: String,
    @SerializedName("day")
    val day: String,
    @SerializedName("evening_doctor")
    val evening_doctor: String,
    @SerializedName("morning_doctor")
    val morning_doctor: String)