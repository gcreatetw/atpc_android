package com.gcreate.atpc.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectDoctorDetail(
    @SerializedName("doctor_clinics")
    val doctor_clinics: List<DoctorClinic>,
    @SerializedName("doctor_educations")
    val doctor_educations: String,
    @SerializedName("doctor_blog")
    val doctor_blog: String,
    @SerializedName("doctor_expertises")
    val doctor_expertises: String,
    @SerializedName("doctor_forensic_medicine_number")
    val doctor_forensic_medicine_number: String,
    @SerializedName("doctor_gender")
    val doctor_gender: String,
    @SerializedName("doctor_licenses")
    val doctor_licenses: String,
    @SerializedName("doctor_name")
    val doctor_name: String,
    @SerializedName("doctor_picture_url")
    val doctor_picture_url: String,
    @SerializedName("doctor_social_media")
    val doctor_social_media: String,
    @SerializedName("doctor_specialist_number")
    val doctor_specialist_number: String)

data class DoctorClinic(
    @SerializedName("clinic_id")
    val clinic_id: String,
    @SerializedName("clinic_name")
    val clinic_name: String)