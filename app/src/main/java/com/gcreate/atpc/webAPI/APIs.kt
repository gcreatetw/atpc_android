package com.gcreate.atpc.webAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface APIs {

    @Headers(*["Content-Type: application/json"])
    @GET("get_clinics_api/v1")
    fun getHomePageInfo(@Query("county") county: String): Call<ApiObjectClinicHomePage>

    @Headers(*["Content-Type: application/json"])
    @GET("get_clinic_info_api/v1")
    fun getClinicDetailInfo(@Query("id") clinicID: Int): Call<ApiObjectClinicDetail>

    @Headers(*["Content-Type: application/json"])
    @GET("get_doctor_info_api/v1")
    fun getDoctorDetailInfo(@Query("id") doctorID: Int): Call<ApiObjectDoctorDetail>

    @Headers(*["Content-Type: application/json"])
    @GET("get_posts/v1")
    fun getArticle(@Query("page") page: Int): Call<ApiObjectArticle>


}