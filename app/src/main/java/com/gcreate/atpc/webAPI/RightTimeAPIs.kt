package com.gcreate.atpc.webAPI

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface RightTimeAPIs {

    @Headers("Content-Type: application/json")
    @GET("clinic/{id}/info")
    fun getRightTimeClinicInfo(@Path("id") clinicID: Int): Call<ApiObjectRightTimeClinicInfo>




}