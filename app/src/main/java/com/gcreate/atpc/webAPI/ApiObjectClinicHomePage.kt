package com.gcreate.atpc.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectClinicHomePage(
    @SerializedName("clinics_info")
    val clinics_info: MutableList<ClinicsInfo>,
    @SerializedName("home_page_banner")
    val home_page_banner: List<String>)

data class ClinicsInfo(
    @SerializedName("clinic_adress")
    val clinic_adress: String,
    @SerializedName("clinic_id")
    val clinic_id: Int,
    @SerializedName("clinic_img_url")
    val clinic_img_url: String,
    @SerializedName("clinic_name")
    val clinic_name: String,
    @SerializedName("clinic_tel")
    val clinic_tel: String)