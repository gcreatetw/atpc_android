package com.gcreate.atpc.webAPI

data class ApiObjectRightTimeClinicInfo(
    val address: String,
    val afternoonFrom: String,
    val afternoonTo: String,
    val announcement: String,
    val clinic_id: Int,
    val contact: String,
    val doctors: List<Doctor>,
    val eveningFrom: String,
    val eveningTo: String,
    val extension: String,
    val intro: String,
    val isDetailOpen: Boolean,
    val isfull: Any,
    val lat: Double,
    val lng: Double,
    val morningFrom: String,
    val morningTo: String,
    val name: String,
    val `open`: Boolean,
    val pics: List<Pic>,
    val room: MutableList<Room>,
    val schedule: Schedule,
    val socialInfo: List<SocialInfo>,
    val type: List<Type>,
    val updated_time: String
)

data class Doctor(
    val education: List<String>,
    val experience: List<String>,
    val name: String,
    val pic: String,
    val sex: String
)

data class Pic(
    val content: Any,
    val seq: Int,
    val title: String,
    val type: String,
    val url: String
)

data class Room(
    val currentDoctor: String,
    val currentNum: Int,
    val name: String,
    val room_id: Int
)

data class Schedule(
    val friday: Friday,
    val monday: Monday,
    val saturday: Saturday,
    val sunday: Sunday,
    val thursday: Thursday,
    val tuesday: Tuesday,
    val wednesday: Wednesday
)

data class SocialInfo(
    val facebook: String,
    val line: Any,
    val website: String
)

data class Type(
    val name: String,
    val type_id: Int
)

data class Friday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>
)

data class Monday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>
)

data class Saturday(
    val afternoon: List<Any>,
    val evening: List<Any>,
    val morning: List<String>
)

data class Sunday(
    val afternoon: List<Any>,
    val evening: List<Any>,
    val morning: List<Any>
)

data class Thursday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>
)

data class Tuesday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<String>
)

data class Wednesday(
    val afternoon: List<String>,
    val evening: List<String>,
    val morning: List<Any>
)