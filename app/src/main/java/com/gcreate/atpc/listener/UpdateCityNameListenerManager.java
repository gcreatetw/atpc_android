package com.gcreate.atpc.listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class UpdateCityNameListenerManager {
    /**
     * 單例模式
     */
    public static UpdateCityNameListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private List<UpdateTextListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static UpdateCityNameListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new UpdateCityNameListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(UpdateTextListener iListener) {
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(UpdateTextListener iListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener);
        }
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast(String cityName) {
        for (UpdateTextListener updateTextListener : iListenerList) {
            updateTextListener.updateSpinnerCityName(cityName);

        }
    }


}

