package com.gcreate.atpc.listener;

public interface WebLoadingListener {
    void notifyViewCancelCall();
}
