package com.gcreate.atpc.listener;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * Android用觀察者模式代替廣播通知刷新界面
 * Reference : https://blog.csdn.net/csm_qz/article/details/46461651
 */

public class RefreshBackgroundListenerManager {
    /**
     * 單例模式
     */
    public static RefreshBackgroundListenerManager listenerManager;

    /**
     * 注册的接口集合，發送廣播的時候都能收到
     */
    private List<RefreshBackgroundListener> iListenerList = new CopyOnWriteArrayList<>();

    /**
     * 獲得單例對象對象
     */
    public static RefreshBackgroundListenerManager getInstance() {
        if (listenerManager == null) {
            listenerManager = new RefreshBackgroundListenerManager();
        }
        return listenerManager;
    }

    /**
     * 注册監聽
     */
    public void registerListener(RefreshBackgroundListener iListener) {
        iListenerList.add(iListener);
    }

    /**
     * 註銷監聽
     */
    public void unRegisterListener(RefreshBackgroundListener iListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener);
        }
    }

    /**
     * 發送廣播
     */
    public void sendBroadCast() {
        for (RefreshBackgroundListener refreshBackground : iListenerList) {
            refreshBackground.refreshBackground();

        }
    }


}

