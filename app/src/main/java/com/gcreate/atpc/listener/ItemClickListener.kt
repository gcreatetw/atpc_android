package com.gcreate.atpc.listener

interface ItemClickListener {
    fun onItemClickListener(position: Int)

}