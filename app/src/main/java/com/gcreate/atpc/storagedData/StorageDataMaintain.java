package com.gcreate.atpc.storagedData;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.gcreate.atpc.webAPI.ApiObjectClinicDetail;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class StorageDataMaintain {

    public static Context context;

    //------------------------------------------- init -------------------------------------------
    public static List<ApiObjectClinicDetail> favClinicList ;

    public static void removeFromFavList(int pos) {
        if (favClinicList != null && favClinicList.size() > pos) {
            favClinicList.remove(pos);
            saveFavList();
            getFavList();
        }
    }

    public static void saveFavList() {
        if (favClinicList != null) {
            SharedPreferences.Editor editor = ((AppCompatActivity) context).getPreferences(MODE_PRIVATE).edit();
            editor.putString("FavClinicList", new Gson().toJson(favClinicList));
            editor.commit();
            getFavList();
        }
    }

    public static void getFavList() {
        favClinicList = new Gson().fromJson(((AppCompatActivity) context).
                getPreferences(MODE_PRIVATE).getString("FavClinicList",null), new TypeToken<ArrayList<ApiObjectClinicDetail>>() {
        }.getType());
        if (favClinicList == null)
            favClinicList = new ArrayList<>();
    }

    public static void addToFavList(ApiObjectClinicDetail clinic) {
        if (favClinicList != null) {
            for (int i = 0; i < favClinicList.size(); i++) {
                if (favClinicList.get(i).getClinic_id() == clinic.getClinic_id()) {
                    favClinicList.remove(i);
                    i--;
                }
            }
        } else {
            favClinicList = new ArrayList<>();
        }
        favClinicList.add(clinic);
        saveFavList();
        getFavList();
    }
}
