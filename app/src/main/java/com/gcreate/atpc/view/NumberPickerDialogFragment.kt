package com.gcreate.atpc.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.navArgs
import com.aigestudio.wheelpicker.WheelPicker.OnWheelChangeListener
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.DialogfragmentNumberPickerBinding
import com.gcreate.atpc.listener.UpdateCityNameListenerManager
import com.gcreate.atpc.model.DataBean


class NumberPickerDialogFragment : DialogFragment() {

    private lateinit var binding: DialogfragmentNumberPickerBinding
    private lateinit var navController: NavController
    private lateinit var typeName: List<String>

    private val args: NumberPickerDialogFragmentArgs by navArgs()
    private var stopPosition: Int? = 0
    private var selectedCity: String? = ""


    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout((WelcomeActivity.windowWidth * 0.8).toInt(), (WelcomeActivity.windowHeight * 0.6).toInt())
            getDialog()!!.window!!.setBackgroundDrawableResource(R.drawable.layout_bg)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialogfragment_number_picker, container, false)
        navController = findNavController(this)

        viewAction()
        cityWheelPicker()

        return binding.root
    }

    private fun viewAction() {
        binding.imgCloseNP.setOnClickListener {
            dismiss()
        }

        binding.btnCheck.setOnClickListener {
//            val args = Bundle()
//            args.putString("cityName", selectedCity)
//            Log.d("ben","selectedCity = " + selectedCity)
            WelcomeActivity.userLocationCity = selectedCity
            UpdateCityNameListenerManager.getInstance().sendBroadCast(selectedCity)
            dialog!!.dismiss()
//            navController.navigate(R.id.action_numberPickerDialogFragment_to_clinicFragment, args)
        }


    }

    private fun cityWheelPicker() {
        typeName =  DataBean.taiwanCity()

        val np = binding.numberPicker
        np.data = typeName
        np.setSelectedItemPosition(currentPosition(), false)
        np.setOnWheelChangeListener(object : OnWheelChangeListener {
            override fun onWheelScrolled(offset: Int) {
            }

            override fun onWheelSelected(position: Int) {
                stopPosition = position
                selectedCity = typeName[position]
            }

            override fun onWheelScrollStateChanged(state: Int) {}
        })

    }

    private fun currentPosition(): Int {
        var selectPosition = 0
        for (i in typeName.indices) {
            if (typeName[i] == args.cityName) selectPosition = i
        }
        return selectPosition
    }



}