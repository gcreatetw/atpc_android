package com.gcreate.atpc.view


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.ActivityWelcomeBinding
import com.gcreate.atpc.util.LocationFinder
import com.gcreate.atpc.util.PdfTool
import com.gcreate.atpc.util.permission.PermissionCheckUtil
import com.gcreate.atpc.webAPI.Web
import com.google.android.gms.location.*
import java.util.*

class WelcomeActivity : AppCompatActivity() {

    private val tag = "ATPC_Log"
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    // Default  定位台北101
    private var latitude = 25.033693818257074
    private var longitude = 121.56490390897176

    // Default Ben house
    //    private var latitude = 24.742026
    //    private var longitude = 121.083828

    companion object {
        var windowWidth = 0
        var windowHeight = 0
        var userLocationCity: String? = "台北市"

        @JvmName("getWindowSize")
        @JvmStatic
        fun getWindowSize(mActivity: Activity) {
            val dm = DisplayMetrics()
            mActivity.windowManager.defaultDisplay.getMetrics(dm)
            windowHeight = dm.heightPixels
            windowWidth = dm.widthPixels
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityWelcomeBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome)

        setHideWindowStatusBar(window)

        binding.cl.background = BitmapDrawable(this.resources, PdfTool().pdfToBitmap(this, R.raw.bg_welcome)[0])
        binding.imageview.setImageBitmap(PdfTool().pdfToBitmap(this, R.raw.logo)[0])

        if (haveInternet()) {

            getWindowSize(this)
            checkPermission()
            Web.initAtpcAPI()
            Web.initRightTimeAPI()

        } else {
            val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this, R.style.AlertDialogTheme2)
            alertDialogBuilder.setTitle("找不到網際網路連線")
            alertDialogBuilder.setMessage("請先開啟網路連線")
            alertDialogBuilder.setPositiveButton("確定") { _, _ ->
                finish()
            }

            val alertDialog = alertDialogBuilder.create()
            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.black222222))
            }
            alertDialog.setCancelable(false)
            alertDialog.show()
        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {

                    val addresses = Geocoder(applicationContext, Locale.getDefault()).getFromLocation(location.latitude , location.longitude , 1)
                    val address = addresses[0]
                    userLocationCity = if (address.subAdminArea != null) {
                        address.subAdminArea
                    } else {
                        address.adminArea
                    }
                }
            }
        }

    }

    /**
     * 隱藏 status bar . 並將 Logo Image 延展至 status bar
     */
    @Suppress("DEPRECATION")
    private fun setHideWindowStatusBar(window: Window) {
        val decorView = window.decorView
        val option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        decorView.systemUiVisibility = option
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = Color.TRANSPARENT
    }

    /**
     * checkPermission
     */
    private var permissionsArray = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    private fun checkPermission() {
        requestPermissions(permissionsArray, PermissionCheckUtil.ONCE_TIME_APPLY)
    }

    /**
     * 请求权限结果回调
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)


        //        GlobalScope.launch(Dispatchers.IO) {
        getUserLocation() //            getCurrentPosition(this@WelcomeActivity)
        //        }

        //  Method 3
        //        startLocation()

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity(Intent(applicationContext, MainActivity::class.java))
            finish()
        }, 2000)
    }

    /** 判斷有無連接網路
     * */
    private fun haveInternet(): Boolean {
        var result = false
        val connManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val info = connManager.activeNetworkInfo
        result = if (info == null || !info.isConnected) {
            false
        } else {
            info.isAvailable
        }
        return result
    }

    /**
     * 定位 method 1. 容易 grpc error
     * */
    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

//        Thread {
//
//            fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
//
//                Log.i(tag, "location is null ? = ${(location != null)} ")
//                Log.i(tag, "location  = $location ")
//                GlobalScope.launch(Dispatchers.IO) {
//                    val addresses: MutableList<Address> = if (location != null) {
//                        Log.i(tag, "get location data")
//                        Log.i(tag, "location.latitude = ${location.latitude}")
//                        Log.i(tag, "location.longitude = ${location.longitude}")
//                        Geocoder(applicationContext, Locale.getDefault()).getFromLocation(location.latitude, location.longitude, 1)
//                    } else {
//
//                        Log.i(tag, "use default location latitude and longitude ")
//                        Log.i(tag, "latitude = $latitude")
//                        Log.i(tag, "longitude = $longitude")
//                        Geocoder(applicationContext, Locale.getDefault()).getFromLocation(latitude, longitude, 1)
//
//                    }
//
//                    /** Reference article
//                     * https://badgameshow.com/fly/android-%E5%88%A9%E7%94%A8%E7%B6%93%E7%B7%AF%E5%BA%A6%E5%8F%96%E5%BE%97%E5%9C%B0%E5%9D%80/fly/android/
//                     * */
//                    val address = addresses[0]
//                    Log.i(tag, "address = $address")
//                    Log.i(tag, "address.adminArea = ${address.adminArea}")
//                    Log.i(tag, "address.subAdminArea = ${address.subAdminArea}")
//                    userLocationCity = if (address.subAdminArea != null) {
//                        address.subAdminArea
//                    } else {
//                        address.adminArea
//                    }
//                }
//            }
//
//        }.start()
        getUserLocation1()
    }

    /**
     * 定位 method 2
     * */
    private fun getCurrentPosition(mContext: Context) {
        val finder = LocationFinder(mContext)
        if (finder.canGetLocation()) {

            if (finder.latitude != 0.0 && finder.longitude != 0.0) {
                latitude = finder.latitude
                longitude = finder.longitude
                Log.i(tag, "inside finder.latitude = ${finder.latitude}")
                Log.i(tag, "inside finder.longitude = ${finder.longitude}")
            }
            Log.i(tag, "finder.latitude = ${finder.latitude}")
            Log.i(tag, "finder.longitude = ${finder.longitude}")
            val addresses = Geocoder(this, Locale.getDefault()).getFromLocation(latitude, longitude, 1)

            /** Reference article
             * https://badgameshow.com/fly/android-%E5%88%A9%E7%94%A8%E7%B6%93%E7%B7%AF%E5%BA%A6%E5%8F%96%E5%BE%97%E5%9C%B0%E5%9D%80/fly/android/
             * */
            val address = addresses[0]
            Log.i(tag, "address = $address")
            Log.i(tag, "address.subAdminArea = ${address.adminArea}")
            if (address.subAdminArea != null) {
                userLocationCity = address.subAdminArea
            } else {
                userLocationCity = address.adminArea
            }


        } else {
            finder.showSettingsAlert()
        }
    }

    /**
     * 定位 method 3
     * */
    private var lm: LocationManager? = null

    @SuppressLint("MissingPermission")
    private fun getGPSLocation() {


        if (!isGpsAble(lm!!)) {
            Toast.makeText(this, "請打開GPS", Toast.LENGTH_SHORT).show()
            openGPS2()
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) { //未開啟定位權限
            //開啟定位權限,200是標識碼
            checkPermission()
        } else {
            startLocation()
            Toast.makeText(this, "已開啟定位權限", Toast.LENGTH_LONG).show();
        }


        val location = lm!!.getLastKnownLocation(LocationManager.GPS_PROVIDER) //        Log.i(tag, "location!!.latitude = ${location!!.latitude} ") //        Log.i(tag, "location!!.longitude  = ${location!!.longitude} ")
    }

    //判斷GPS服務是否開啟
    private fun isGpsAble(lm: LocationManager): Boolean {
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    //打開設定頁面讓用戶自己設定
    private fun openGPS2() {
        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
        startActivityForResult(intent, 0)
    }

    @SuppressLint("MissingPermission")
    private fun startLocation() {
        lm = getSystemService(LOCATION_SERVICE) as LocationManager
        val lc = lm!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        lm!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 8f, object : LocationListener {
            override fun onLocationChanged(location: Location) { // 當GPS定位資訊發生改變時，更新定位
                Log.i(tag, "location!!.latitude = ${location.latitude} ")
                Log.i(tag, "location!!.longitude  = ${location.longitude} ")
            }

            override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

            override fun onProviderEnabled(provider: String) { // 當GPS LocationProvider可用時，更新定位

            }

            override fun onProviderDisabled(provider: String) {
            }
        })
    }

    /**
     * 定位 method 4
     * */
    @SuppressLint("MissingPermission")
    private fun getUserLocation1() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.requestLocationUpdates(LocationRequest.create() , locationCallback , Looper.getMainLooper())
    }


    override fun onDestroy() {
        super.onDestroy()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}











