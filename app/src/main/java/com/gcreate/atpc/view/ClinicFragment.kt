package com.gcreate.atpc.view

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide.with
import com.gcreate.atpc.R
import com.gcreate.atpc.adapter.ClinicListAdapter
import com.gcreate.atpc.databinding.FragmentClinicBinding
import com.gcreate.atpc.listener.ItemClickListener
import com.gcreate.atpc.listener.UpdateCityNameListenerManager
import com.gcreate.atpc.listener.UpdateTextListener
import com.gcreate.atpc.model.DataBean
import com.gcreate.atpc.util.PdfTool
import com.gcreate.atpc.webAPI.ApiObjectClinicHomePage
import com.gcreate.atpc.webAPI.ClinicsInfo
import com.gcreate.atpc.webAPI.Web
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * 院所
 */


class ClinicFragment : Fragment(), UpdateTextListener {

    private val logcatTag = "ATPC_Log"

    private lateinit var binding: FragmentClinicBinding
    private lateinit var navController: NavController
    private lateinit var call: Call<ApiObjectClinicHomePage>


    companion object {
        var selectedCityName: String = ""
        var isFirsTimeInHomePage: Boolean = true

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic, container, false)
        navController = findNavController(this)
        UpdateCityNameListenerManager.getInstance().registerListener(this)

        Log.d(logcatTag, "WelcomeActivity.userLocationCity = " + WelcomeActivity.userLocationCity)

        initView()
        viewAction()
        getHomePageApiData()

        return binding.root

    }

    private fun initView() {

        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(activity as Context, R.raw.logo)[0])
        if (isFirsTimeInHomePage) {
            binding.tvCityNumberPicker.text = WelcomeActivity.userLocationCity
            selectedCityName = WelcomeActivity.userLocationCity!!
        } else {
            binding.tvCityNumberPicker.text = selectedCityName
        }

        binding.rvClinicList.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

    }

    private fun viewAction() {
        binding.clinicCityNp.setOnClickListener {
            val args = Bundle()
            args.putString("cityName", binding.tvCityNumberPicker.text.toString())
            navController.navigate(R.id.action_clinicFragment_to_numberPickerDialogFragment, args)
        }
    }

    /** Get server data*/
    private fun getHomePageApiData() {

        val cityName: String
        if (isFirsTimeInHomePage) {
            isFirsTimeInHomePage = false
            cityName = WelcomeActivity.userLocationCity!!
        } else {
            cityName = selectedCityName
        }

        call =  Web.ATPC_API.getHomePageInfo(cityName)
        call.enqueue(object : Callback<ApiObjectClinicHomePage> {
            override fun onResponse(call: Call<ApiObjectClinicHomePage>, response: Response<ApiObjectClinicHomePage>) {
                val objectClinicHomePage = response.body()!!
                initBanner(objectClinicHomePage.home_page_banner)
                bindClinicRecyclerVIewData(objectClinicHomePage.clinics_info)
            }

            override fun onFailure(call: Call<ApiObjectClinicHomePage>, t: Throwable) {
                initBanner(null)
            }
        })
    }

    /** Banner */
    private fun initBanner(bannerList: List<String>?) {
        val mBanner = binding.componentBanner.banner
        mBanner.layoutParams.height = ((WelcomeActivity.windowHeight * 0.25).toInt()) //  Default Image
        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(activity as Context, R.raw.banner_default)[0])
        val dataList: MutableList<DataBean>

        if (!bannerList.isNullOrEmpty()) {
            dataList = ArrayList()
            for (element in bannerList) {
                dataList.add(DataBean(element, null, 1))
            }
        } else {
            dataList = DataBean.bannerImages()
        }

        /*
                * 内置的PageTransformer
                * AlphaPageTransformer  /   DepthPageTransformer    /   RotateDownPageTransformer   /   RotateUpPageTransformer
                * RotateYTransformer /  ScaleInTransformer   /  ZoomOutPageTransformer
                * */

        // 设置图片加载器
        mBanner.setAdapter(object : BannerImageAdapter<DataBean>(dataList) {
            override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
                with(holder.itemView).load(dataList[position].imageUrl).error(dm).into(holder.imageView)
            }
        }).addBannerLifecycleObserver(this).indicator = CircleIndicator(activity)

        // 輪播方式
        mBanner.setPageTransformer(AlphaPageTransformer()) // Indicator parameters
            .setIndicatorSelectedColor(ContextCompat.getColor(activity as Context, R.color.green1F9A28)).setIndicatorSpace(16)
            .setIndicatorWidth(15, 15).setLoopTime(5000).start()

    }

    /** RecyclerView*/
    private fun bindClinicRecyclerVIewData(dataList: MutableList<ClinicsInfo>) {

        if (dataList.isNullOrEmpty()) {
            binding.rvClinicList.visibility = View.GONE
            binding.imgNoClinic.visibility = View.VISIBLE
        } else {
            binding.rvClinicList.visibility = View.VISIBLE
            binding.imgNoClinic.visibility = View.GONE
        }

        val adapter = ClinicListAdapter(activity as FragmentActivity, R.layout.card_clinic, dataList)
        binding.rvClinicList.adapter = adapter

        adapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                Log.d("ben","dataList[position].clinic_id = " + dataList[position].clinic_id)
                val action = ClinicFragmentDirections.actionClinicFragmentToClinicInfoFragment(dataList[position].clinic_id)
                navController.navigate(action)
            }
        })
    }

    override fun updateSpinnerCityName(cityName: String) {
        selectedCityName = cityName
        binding.tvCityNumberPicker.text = cityName
        getHomePageApiData()
    }

    override fun onStop() {
        super.onStop()
        selectedCityName = binding.tvCityNumberPicker.text.toString()
        call.cancel()
        UpdateCityNameListenerManager.getInstance().unRegisterListener(this)
    }

}