package com.gcreate.atpc.view

import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.*
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.atpc.R
import com.gcreate.atpc.adapter.ClinicInfoDoctorAvatarAdapter
import com.gcreate.atpc.adapter.ClinicRoomAdapter
import com.gcreate.atpc.databinding.FragmentClinicInfoBinding
import com.gcreate.atpc.listener.ItemClickListener
import com.gcreate.atpc.storagedData.StorageDataMaintain
import com.gcreate.atpc.util.PdfTool
import com.gcreate.atpc.webAPI.ApiObjectClinicDetail
import com.gcreate.atpc.webAPI.ApiObjectRightTimeClinicInfo
import com.gcreate.atpc.webAPI.Web
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class ClinicInfoFragment : Fragment(), RadioGroup.OnCheckedChangeListener {

    private lateinit var binding: FragmentClinicInfoBinding
    private lateinit var navController: NavController
    private lateinit var objectClinicDetail: ApiObjectClinicDetail
    private lateinit var mActivity: FragmentActivity

    private val args: ClinicInfoFragmentArgs by navArgs()
    private var isFav: Boolean = false
    private var isServerDataLoadComplete = false

    private val linePackageName = "jp.naver.line.android"

    override fun onStart() {
        super.onStart()

        mActivity = requireActivity()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_clinic_info, container, false)
        navController = NavHostFragment.findNavController(this)
        binding.clinicDoctorCalendar.segmentedTab.setOnCheckedChangeListener(this)

        adjudgeInFavList()
        toolbarView()

        getClinicDetailInfo()

        refreshClinicRoomNumber()

        return binding.root
    }

    /*  Toolbar Setting */
    private fun toolbarView() {
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar.materialToolbar)
        setHasOptionsMenu(true)
        binding.toolbar.materialToolbar.title = ""
        binding.toolbar.materialToolbar.inflateMenu(R.menu.toolbar_iteml)
        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(activity as Context, R.raw.logo)[0])
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { (activity as FragmentActivity).onBackPressed() }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_iteml, menu)
        if (isFav) menu.findItem(R.id.action_add_fav).setIcon(R.drawable.icon_like)
        else menu.findItem(R.id.action_add_fav).setIcon(R.drawable.icon_unlike)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_add_fav -> isFav = if (isFav) {
                var i = 0
                while (i < StorageDataMaintain.favClinicList.size) {
                    if (StorageDataMaintain.favClinicList[i].clinic_id == args.clinicID) StorageDataMaintain.removeFromFavList(i)
                    i++
                }
                item.setIcon(R.drawable.icon_unlike)
                !isFav
            } else { //  未加入最愛 - > 加到最愛清單  , 黑愛心-> 紅愛心.
                if (isServerDataLoadComplete) {
                    StorageDataMaintain.addToFavList(objectClinicDetail)
                    item.setIcon(R.drawable.icon_like)
                    !isFav
                } else {
                    isFav
                }

            }
        }

        return super.onOptionsItemSelected(item)
    }

    /*  Get  server data */
    private fun getClinicDetailInfo() {

        Web.ATPC_API.getClinicDetailInfo(args.clinicID).enqueue(object : Callback<ApiObjectClinicDetail> {
            override fun onResponse(call: Call<ApiObjectClinicDetail>, response: Response<ApiObjectClinicDetail>) {
                objectClinicDetail = response.body()!!
                isServerDataLoadComplete = true

                setView()                       //  診所基本資訊
                setDayOfWeekInCalendar()        //  排班表
                setDoctorAvatarRecyclerView()   //  診所醫生

                if (objectClinicDetail.is_use_right_time_system) {
                    getRightTimeRoomNumber()        //  Call Right Time API 取得診間號碼
                }

            }

            override fun onFailure(call: Call<ApiObjectClinicDetail>, t: Throwable) {
                Toast.makeText(activity, "Load server data fail", Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setView() {

        //  診所圖片
        if (objectClinicDetail.clinic_img_url.isEmpty()) {
            val pic: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(activity as Context, R.raw.clinic_pic_default)[0])
            Glide.with(mActivity).load(pic).into(binding.clinicInfoPic.picture)
        } else {
            Glide.with(mActivity).load(objectClinicDetail.clinic_img_url).into(binding.clinicInfoPic.picture)
        }

        binding.tvClinicPhone.text = objectClinicDetail.clinic_tel          //  電話
        binding.tvClinicAddress.text = objectClinicDetail.clinic_address    //  地址

        binding.tvClinicUrl.text = objectClinicDetail.clinic_website        //  網址
        binding.tvClinicUrl.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", objectClinicDetail.clinic_website)
            startActivity(intent)
        }

        binding.btnFb.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", objectClinicDetail.clinic_fb)
            startActivity(intent)
        }

        binding.btnLine.setOnClickListener {

            if (isAppInstalled(mActivity, linePackageName)) {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(objectClinicDetail.clinic_line))
                startActivity(intent)
            } else {
                Toast.makeText(activity, "please install Line app", Toast.LENGTH_LONG).show()
            }
        }

        binding.clinicDoctorCalendar.segmentedTab.setTintColor(ContextCompat.getColor(mActivity, R.color.white), ContextCompat.getColor(mActivity, R.color.green1F9A28))
        binding.rvClinicRoom.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
    }

    /**  排班表 Function */
    private fun setDayOfWeekInCalendar() {

        val calendar = Calendar.getInstance()
        val segmentedTab = binding.clinicDoctorCalendar.segmentedTab

        when (calendar[Calendar.DAY_OF_WEEK]) {
            Calendar.MONDAY -> segmentedTab.check(R.id.radio_monday)
            Calendar.TUESDAY -> segmentedTab.check(R.id.radio_tuesday)
            Calendar.WEDNESDAY -> segmentedTab.check(R.id.radio_wednesday)
            Calendar.THURSDAY -> segmentedTab.check(R.id.radio_thursday)
            Calendar.FRIDAY -> segmentedTab.check(R.id.radio_friday)
            Calendar.SATURDAY -> segmentedTab.check(R.id.radio_saturday)
            Calendar.SUNDAY -> segmentedTab.check(R.id.radio_sunday)
        }
    }

    private fun setDoctorNameOnSchedule(day: Int) {
        val calendar = binding.clinicDoctorCalendar
        /** 開診時間*/
        calendar.tvClinicOpenTimeMorning.text = objectClinicDetail.business_hours.morning
        calendar.tvClinicOpenTimeAfternoon.text = objectClinicDetail.business_hours.afternoon
        calendar.tvClinicOpenTimeNight.text = objectClinicDetail.business_hours.evening

        when (day) {
            Calendar.MONDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[0].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[0].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[0].evening_doctor)
                return
            }

            Calendar.TUESDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[1].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[1].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[1].evening_doctor)
                return
            }

            Calendar.WEDNESDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[2].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[2].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[2].evening_doctor)
                return
            }

            Calendar.THURSDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[3].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[3].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[3].evening_doctor)
                return
            }

            Calendar.FRIDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[4].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[4].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[4].evening_doctor)
                return
            }

            Calendar.SATURDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[5].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[5].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[5].evening_doctor)
                return
            }

            Calendar.SUNDAY -> {
                calendar.tvClinicDoctorMorning.text = spiltString(objectClinicDetail.duty_doctors[6].morning_doctor)
                calendar.tvClinicDoctorAfternoon.text = spiltString(objectClinicDetail.duty_doctors[6].afternoon_doctor)
                calendar.tvClinicDoctorNight.text = spiltString(objectClinicDetail.duty_doctors[6].evening_doctor)
                return
            }
        }
    }

    private fun spiltString(Doctors: String): String {

        val separated: List<String> = Doctors.split(",")
        var str = ""
        for (i: Int in separated.indices) {
            if (separated.size == 1 || i == separated.size - 1) {
                str += separated[i]
            } else {
                str = str + separated[i] + '\n'
            }
        }
        return str


    }

    override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

        when (checkedId) {
            R.id.radio_monday -> {
                setDoctorNameOnSchedule(Calendar.MONDAY)
                return
            }
            R.id.radio_tuesday -> {
                setDoctorNameOnSchedule(Calendar.TUESDAY)
                return
            }
            R.id.radio_wednesday -> {
                setDoctorNameOnSchedule(Calendar.WEDNESDAY)
                return
            }
            R.id.radio_thursday -> {
                setDoctorNameOnSchedule(Calendar.THURSDAY)
                return
            }
            R.id.radio_friday -> {
                setDoctorNameOnSchedule(Calendar.FRIDAY)
                return
            }
            R.id.radio_saturday -> {
                setDoctorNameOnSchedule(Calendar.SATURDAY)
                return
            }
            R.id.radio_sunday -> {
                setDoctorNameOnSchedule(Calendar.SUNDAY)
                return
            }
        }
    }

    /**  Doctor head avatar recyclerview*/
    private fun setDoctorAvatarRecyclerView() {
        if (objectClinicDetail.clinic_doctors.isEmpty()) {
            binding.tvDoctorListTitle.visibility = View.GONE
        } else {
            binding.tvDoctorListTitle.visibility = View.VISIBLE
        }

        binding.rvClinicDoctorAvatar.layoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false)

        val adapter = ClinicInfoDoctorAvatarAdapter(mActivity, R.layout.card_doctor_avatar, objectClinicDetail.clinic_doctors)
        binding.rvClinicDoctorAvatar.adapter = adapter

        adapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                val args = Bundle()
                args.putInt("doctorID", objectClinicDetail.clinic_doctors[position].clinic_doctor_id)
                navController.navigate(R.id.action_clinicInfoFragment_to_doctorDetailFragment, args)
            }
        })
    }

    /** Doctor clinic room*/
    private fun getRightTimeRoomNumber() {
        Web.RightTime_API.getRightTimeClinicInfo(objectClinicDetail.right_time_clinic_id.toInt())
            .enqueue(object : Callback<ApiObjectRightTimeClinicInfo> {
                override fun onResponse(call: Call<ApiObjectRightTimeClinicInfo>, response: Response<ApiObjectRightTimeClinicInfo>) {
                    val objectRightTimeClinicInfo = response.body()!!

                    val adapter = ClinicRoomAdapter(activity as FragmentActivity, R.layout.card_clinic_room, objectRightTimeClinicInfo.room)
                    binding.rvClinicRoom.adapter = adapter
                }

                override fun onFailure(call: Call<ApiObjectRightTimeClinicInfo>, t: Throwable) {
                    TODO("Not yet implemented")
                }
            })
    }

    /** check the app is installed*/
    private fun isAppInstalled(context: Context, packageName: String): Boolean {
        var packageInfo: PackageInfo?
        try {
            packageInfo = context.packageManager.getPackageInfo(packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            packageInfo = null
            e.printStackTrace()
        }
        return packageInfo != null
    }

    /** 每 20 秒更新門診號碼*/
    private val handler = Handler(Looper.getMainLooper())
    private val task: Runnable = object : Runnable {
        override fun run() {
            handler.postDelayed(this, (20 * 1000).toLong()) //设置延迟时间，此处是20秒
            getClinicDetailInfo()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handler.postDelayed(task, 20000) //延迟调用
        handler.post(task) //立即调用
    }

    override fun onStop() {
        super.onStop()
        handler.removeCallbacks(task) //立即调用
    }

    /** Refresh View*/
    private fun refreshClinicRoomNumber() {
        binding.refreshView.setOnRefreshListener { //需要执行的代码 ，更新診間號碼
            if (objectClinicDetail.right_time_clinic_id.isNotEmpty()) {
                getRightTimeRoomNumber()
            }

            binding.refreshView.isRefreshing = false
        }
    }

    /** 判斷診所是否被加進最愛清單*/
    private fun adjudgeInFavList() {
        val dataList = StorageDataMaintain.favClinicList
        val allFavClinicID = mutableListOf<Int>()

        for (i: Int in 0 until dataList.size) {
            allFavClinicID.add(dataList[i].clinic_id)
        }
        isFav = dataList.size != 0 && allFavClinicID.contains(args.clinicID)
    }

}


