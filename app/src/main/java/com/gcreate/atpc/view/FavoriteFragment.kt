package com.gcreate.atpc.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.atpc.R
import com.gcreate.atpc.adapter.Callback_stockListItemSwipeToDelete
import com.gcreate.atpc.adapter.ClinicFavListAdapter
import com.gcreate.atpc.databinding.FragmentFavoriteBinding
import com.gcreate.atpc.listener.RefreshBackgroundListener
import com.gcreate.atpc.listener.RefreshBackgroundListenerManager
import com.gcreate.atpc.storagedData.StorageDataMaintain
import com.gcreate.atpc.util.PdfTool


class FavoriteFragment : Fragment(), RefreshBackgroundListener {

    private lateinit var binding: FragmentFavoriteBinding
    private lateinit var navController: NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorite, container, false)
        navController = NavHostFragment.findNavController(this)
        RefreshBackgroundListenerManager.getInstance().registerListener(this)

        initView()

        return binding.root
    }

    private fun initView() {
        backGroundShowOrHide()
        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(activity as Context, R.raw.logo)[0])

        //  RecyclerView Data Binding
        binding.rvContent.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val adapter = ClinicFavListAdapter(activity as FragmentActivity, binding, StorageDataMaintain.favClinicList)
        binding.rvContent.adapter = adapter

        adapter.setOnItemClickListener(object : ClinicFavListAdapter.OnItemClickListener {
            override fun onItemClick(view: View?, position: Int) {
                val action = FavoriteFragmentDirections.actionFavoriteFragmentToClinicInfoFragment(StorageDataMaintain.favClinicList[position].clinic_id)
                navController.navigate(action)
            }

            //            override fun onItemClickListener(position: Int) {
            //                ClinicFragment.setClinicObject(dataList[position])
            //                val action = ClinicFragmentDirections.actionClinicFragmentToClinicInfoFragment(dataList[position].clinic_id)
            //                navController.navigate(action)
            //            }
        })

        val itemTouchHelper = ItemTouchHelper(Callback_stockListItemSwipeToDelete(requireActivity(), adapter))
        itemTouchHelper.attachToRecyclerView(binding.rvContent)
    }

    private fun backGroundShowOrHide() { //  有無收藏最愛診所
        if (StorageDataMaintain.favClinicList.isNotEmpty()) {
            binding.rvContent.visibility = View.VISIBLE
            binding.tvWithoutFav.visibility = View.GONE
        } else {
            binding.rvContent.visibility = View.GONE
            binding.tvWithoutFav.visibility = View.VISIBLE
        }
    }

    override fun refreshBackground() {
        backGroundShowOrHide()
    }

    override fun onStop() {
        super.onStop()
        RefreshBackgroundListenerManager.getInstance().unRegisterListener(this)
    }
}