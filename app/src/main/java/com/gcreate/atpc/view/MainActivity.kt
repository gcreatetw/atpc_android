package com.gcreate.atpc.view

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.ActivityMainBinding
import com.gcreate.atpc.storagedData.StorageDataMaintain

class MainActivity : AppCompatActivity() {
    var FireBaseToken = ""
    private lateinit var uuid  :String
    private lateinit var binding: ActivityMainBinding




    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        uuid = android.provider.Settings.Secure.getString(contentResolver, android.provider.Settings.Secure.ANDROID_ID)

        StorageDataMaintain.context = this
        StorageDataMaintain.getFavList()

        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupWithNavController(binding.bottomNavigationView, navController)

    }

//    private fun initWebAPI() {
////        FireBaseToken = FirebaseMessaging.getInstance().token.toString()
//        val interceptor = HttpLoggingInterceptor()
//        interceptor.level = HttpLoggingInterceptor.Level.BODY
//        val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
//            run {
//                val builder = chain.request().newBuilder().header("Device-ID", UUID)
////                    .header("FCM-token", FireBaseToken)
//                val build = builder.build()
//                chain.proceed(build)
//            }
//            //                                .header("sessionId", "155056366467311249");
//        }.connectTimeout(3, TimeUnit.SECONDS).readTimeout(3, TimeUnit.SECONDS).build()
//        val retrofit: Retrofit = Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(SERVER_IP)
//            .client(client) //Only for Web API debug
//            .build()
//        APIs = retrofit.create(APIs::class.java)
//
//        //Log.d("Firebase", "Token: " + FBToken);
//    }



}