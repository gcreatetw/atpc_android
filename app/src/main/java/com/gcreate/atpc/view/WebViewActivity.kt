package com.gcreate.atpc.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebSettings
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.ActivityWebViewBinding
import com.gcreate.atpc.listener.WebLoadingListener
import com.gcreate.atpc.listener.WebLoadingListenerManager
import com.gcreate.atpc.util.PdfTool

class WebViewActivity : AppCompatActivity(), WebLoadingListener {

    private var url: String? = null
    private var loadingDialog: LoadingDialog? = null
    private lateinit var binding : ActivityWebViewBinding


    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_web_view)
        WebLoadingListenerManager.getInstance().registerListener(this)

        // Loading Animate
        loadingDialog = LoadingDialog()
        loadingDialog!!.show(this.supportFragmentManager, "loading dialog")


        //獲得值
        val intent = intent
        //傳值
        if (intent.getStringExtra("LinkURL") != null) {
            url = intent.getStringExtra("LinkURL")
        }

        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(this, R.raw.logo)[0])
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { onBackPressed() }

        // 開啟網頁設定
        //支持javascript
        binding.webview.settings.javaScriptEnabled = true

        // 設置可以支持缩放
        binding.webview.settings.setSupportZoom(true)
        binding.webview.settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        binding.webview.webViewClient = WebViewClient()
        binding.webview.loadUrl(url!!)



    }

    override fun notifyViewCancelCall() {
        loadingDialog!!.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        WebLoadingListenerManager.getInstance().unRegisterListener(this)
    }

}