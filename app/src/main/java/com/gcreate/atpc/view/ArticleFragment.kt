package com.gcreate.atpc.view

import android.content.Context
import android.content.Intent
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.atpc.R
import com.gcreate.atpc.adapter.ArticleAdapter
import com.gcreate.atpc.databinding.FragmentArticleBinding
import com.gcreate.atpc.model.DataBean
import com.gcreate.atpc.util.PdfTool
import com.gcreate.atpc.webAPI.ApiObjectArticle
import com.gcreate.atpc.webAPI.Post
import com.gcreate.atpc.webAPI.Web
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * 文章
 */
class ArticleFragment : Fragment() {

    private lateinit var binding: FragmentArticleBinding
    private lateinit var navController: NavController
    private lateinit var objectArticle: ApiObjectArticle
    private lateinit var mAdapter: ArticleAdapter
    private lateinit var call: Call<ApiObjectArticle>

    private var dataList: ArrayList<Post> = ArrayList()
    private var currentPage: Int = 1


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_article, container, false)
        navController = NavHostFragment.findNavController(this)

        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(activity as Context, R.raw.logo)[0])

        initView()
        callArticleAPI()

        return binding.root
    }


    /*  Doctor head avatar recyclerview*/
    private fun initView() {
        // recycler view Data binding
        binding.rvArticle.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        mAdapter = ArticleAdapter(requireActivity(), R.layout.card_article, dataList)
        binding.rvArticle.adapter = mAdapter

        // 获取模块
        mAdapter.loadMoreModule
        // 打开或关闭加载更多功能（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMore = true
        // 是否自定加载下一页（默认为true）
        mAdapter.loadMoreModule.isAutoLoadMore = true
        // 当数据不满一页时，是否继续自动加载（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false
        // 所有数据加载完成后，是否允许点击（默认为false）
        mAdapter.loadMoreModule.enableLoadMoreEndClick = false
        // 是否处于加载中
        mAdapter.loadMoreModule.isLoading

        mAdapter.setOnItemClickListener { _, _, position ->
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", dataList[position].post_url)
            startActivity(intent)
        }
    }

    private fun callArticleAPI() {
        call =   Web.ATPC_API.getArticle(currentPage)
        call.enqueue(object : Callback<ApiObjectArticle> {
            override fun onResponse(call: Call<ApiObjectArticle>, response: Response<ApiObjectArticle>) {
                objectArticle = response.body()!!

                initBanner(objectArticle.posts_page_banner)

                if (objectArticle.posts.size < 20) {
                    currentPage--
                    /******************************** 状态设置 ********************************/ // 当前这次数据加载完毕，调用此方法
                    mAdapter.loadMoreModule.loadMoreComplete() // 所有数据加载完成，调用此方法
                    // 需要重置"加载完成"状态时，请调用 setNewData()
                    mAdapter.loadMoreModule.loadMoreEnd()

                }

                populateData(objectArticle)
            }

            override fun onFailure(call: Call<ApiObjectArticle>, t: Throwable) {

            }
        })
    }

    private fun populateData(objectArticle: ApiObjectArticle) {

        for (i in 0 until objectArticle.posts.size) {
            dataList.add(objectArticle.posts[i])
        }

        mAdapter.setNewInstance(dataList)
        mAdapter.notifyItemRangeChanged(dataList.size - objectArticle.posts.size, dataList.size - 1)
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false

        mAdapter.loadMoreModule.setOnLoadMoreListener {
            currentPage++
            callArticleAPI()
        }

        // 预加载的位置（默认为1）
        mAdapter.loadMoreModule.preLoadNumber = currentPage

    }

    private fun initBanner(bannerList: List<String>?) {
        val mBanner = binding.componentBanner.banner
        mBanner.layoutParams.height = ((WelcomeActivity.windowHeight * 0.25).toInt())
        val dm: Drawable = BitmapDrawable(resources, PdfTool().pdfToBitmap(activity as Context, R.raw.banner_default)[0])
        val dataList: MutableList<DataBean>

        if (!bannerList.isNullOrEmpty()) {
            dataList = ArrayList()
            for (element in bannerList) {
                dataList.add(DataBean(element, null, 1))
            }
        } else {
            dataList = DataBean.bannerImages()
        }

        /*
                * 内置的PageTransformer
                * AlphaPageTransformer  /   DepthPageTransformer    /   RotateDownPageTransformer   /   RotateUpPageTransformer
                * RotateYTransformer /  ScaleInTransformer   /  ZoomOutPageTransformer
                * */ //设置图片加载器
        mBanner.setAdapter(object : BannerImageAdapter<DataBean>(dataList) {
            override fun onBindView(holder: BannerImageHolder, data: DataBean, position: Int, size: Int) {
                Glide.with(holder.itemView).load(dataList[position].imageUrl).error(dm).into(holder.imageView)

            }
        }).addBannerLifecycleObserver(this).indicator = CircleIndicator(activity) // 輪播方式
        mBanner.setPageTransformer(AlphaPageTransformer()) // Indicator parameters
            .setIndicatorSelectedColor(ContextCompat.getColor(activity as Context, R.color.green1F9A28)).setIndicatorSpace(16)
            .setIndicatorWidth(15, 15) // Indicator parameters
            .start()

    }

    override fun onStop() {
        super.onStop()
        call.cancel()
    }
}