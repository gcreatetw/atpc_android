package com.gcreate.atpc.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.gcreate.atpc.R
import com.gcreate.atpc.databinding.FragmentDoctorDetailBinding
import com.gcreate.atpc.util.PdfTool
import com.gcreate.atpc.webAPI.ApiObjectDoctorDetail
import com.gcreate.atpc.webAPI.Web
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DoctorDetailFragment : Fragment() {

    private lateinit var binding: FragmentDoctorDetailBinding
    private lateinit var navController: NavController
    private lateinit var objectDoctorDetail: ApiObjectDoctorDetail

    private val args: DoctorDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_doctor_detail, container, false)
        navController = NavHostFragment.findNavController(this)

        toolbarView()
        getServerData()

        return binding.root
    }


    /** Toolbar Setting */
    private fun toolbarView() {
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar.materialToolbar)
        setHasOptionsMenu(true)
        binding.toolbar.materialToolbar.title = ""
        binding.toolbar.materialToolbar.inflateMenu(R.menu.toolbar_iteml)
        binding.toolbar.toolbarLogo.setImageBitmap(PdfTool().pdfToBitmap(activity as Context, R.raw.logo)[0])
        binding.toolbar.materialToolbar.setNavigationIcon(R.drawable.icon_back)
        binding.toolbar.materialToolbar.setNavigationOnClickListener { (activity as FragmentActivity).onBackPressed() }
    }

    /** Ger doctor server Api data*/
    private fun getServerData() {

        Web.ATPC_API.getDoctorDetailInfo(args.doctorID).enqueue(object : Callback<ApiObjectDoctorDetail> {
            override fun onResponse(call: Call<ApiObjectDoctorDetail>, response: Response<ApiObjectDoctorDetail>) {
                objectDoctorDetail = response.body()!!

                if (objectDoctorDetail.doctor_social_media.isEmpty()){
                    binding.btnFb.visibility = View.INVISIBLE
                }else{
                    binding.btnFb.visibility = View.VISIBLE
                }

                if (objectDoctorDetail.doctor_blog.isEmpty()){
                    binding.btnBlog.visibility = View.INVISIBLE
                }else{
                    binding.btnBlog.visibility = View.VISIBLE
                }

                setView()
            }

            override fun onFailure(call: Call<ApiObjectDoctorDetail>, t: Throwable) {
                Toast.makeText(activity, "Load server data fail", Toast.LENGTH_SHORT).show()
            }
        })
    }

    /** Bind data to View*/
    private fun setView() {

        //  醫生姓名
        binding.doctorDetailName.text = String.format(requireActivity().resources.getString(R.string.text_name), objectDoctorDetail.doctor_name)
        //  醫生性別
        binding.doctorDetailGender.text = String.format(requireActivity().resources.getString(R.string.text_gender), objectDoctorDetail.doctor_gender)
        //  專精字號 (精專醫字號 + 法精醫字號)
        val specialListNumber =  String.format(requireActivity().resources.getString(R.string.text_specialist_number), objectDoctorDetail.doctor_specialist_number)
        val forensicMedicineNumber = String.format(requireActivity().resources.getString(R.string.text_forensic_medicine_number), objectDoctorDetail.doctor_forensic_medicine_number)
        val certificationNumber =  specialListNumber + '\n' + forensicMedicineNumber
        binding.doctorDetailCertificationNumber.text = certificationNumber
        //  證書
        binding.doctorDetailCertificationName.text = spiltString(objectDoctorDetail.doctor_licenses)
        //  醫師頭像
        Glide.with(requireActivity()).load(objectDoctorDetail.doctor_picture_url).into(binding.imgDoctorAvatar)
        //  專長
        if (objectDoctorDetail.doctor_expertises.isNotEmpty()){
            binding.doctorExpertises.text = spiltString(objectDoctorDetail.doctor_expertises)
        }
        //  經歷
        if (objectDoctorDetail.doctor_educations.isNotEmpty()){
            binding.doctorEducations.text = spiltString(objectDoctorDetail.doctor_educations)
        }

        binding.btnFb.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", objectDoctorDetail.doctor_social_media)
            startActivity(intent)
        }

        binding.btnBlog.setOnClickListener {
            val intent = Intent(context, WebViewActivity::class.java)
            intent.putExtra("LinkURL", objectDoctorDetail.doctor_blog)
            startActivity(intent)
        }

    }

    private fun spiltString(Doctors: String): String {
        val separated: List<String> = Doctors.split(",")
        val symbol = 183.toChar()
        var str = symbol.toString()
        for (i: Int in separated.indices) {
            if (separated.size == 1 || i == separated.size - 1) {
                str += separated[i]
            } else {
                str = str + separated[i] + '\n' + "‧"
            }
        }
        return str
    }


}